import random
import glob, os

random.seed(9001)


def shuffle(startingFolders): 
    rootFolders = []
    rootFoldersBackup = []
    nFiles = 0
    for startingFolder in startingFolders:
        newFolder = glob.glob(startingFolder + "/*") 
        rootFolders += newFolder
        rootFoldersBackup += newFolder
        nFiles += len(get_all_files(startingFolder))
    
    dict = {}
    for rf in rootFolders:
        dict[rf] = get_all_files(rf)
    
    backupFile = open("backup.txt", "w")
    
    filesMoved= 0
    latestChosenFolder = ""
    while filesMoved < nFiles:        
        randomFolder = random.choice(rootFolders)
        
        if (randomFolder == latestChosenFolder):
            randomFolder = random.choice(rootFolders)
            
        latestChosenFolder = randomFolder
        filesFromRandomFolder = dict[randomFolder]
        
        if filesFromRandomFolder != []:
            randomFileToMove = random.choice(filesFromRandomFolder)
            move_and_backup(randomFileToMove, backupFile, filesMoved, nFiles)
            dict[randomFolder].remove(randomFileToMove)        
            filesMoved += 1
        rootFolders.remove(latestChosenFolder)
        if (rootFolders == []):
            for bkpRf in rootFoldersBackup:
                rootFolders.append(bkpRf)
    
    backupFile.close()


def unshuffle():
    sortedFiles = glob.glob("SORTEADOS/*")
    nFiles = len(sortedFiles)
    
    if nFiles > 0:
        print("Reverting changes...")
        originalPaths = []
        with open("backup.txt", "r") as ins:
            for line in ins:
                originalPaths.append(line.strip("\n"))
                
        currentFileIdx = 1        
        for modifiedPath, originalPath in zip(sortedFiles, originalPaths):
            print("(" + str(currentFileIdx) + "/" + str(nFiles) + "):" + os.path.basename(modifiedPath) + " -> "+  os.path.basename(originalPath))
            os.rename(modifiedPath,originalPath)
            currentFileIdx += 1
        os.remove("backup.txt")
        print("Done\n") 
    
    allFiles = [] 
        
def move_and_backup(pathOfFileToMove, backupFile, prefixNumber, largestNumber):
    basename = os.path.basename(pathOfFileToMove)
    zeroesToFill = len(str(largestNumber))
    newFileName = str(prefixNumber).zfill(zeroesToFill) +  " - " + basename
    print("(" + str(prefixNumber + 1) + "/" + str(largestNumber) + "):" + newFileName)
    backupFile.write("%s\n" % pathOfFileToMove)
    os.rename(pathOfFileToMove,"SORTEADOS\\" + newFileName)    
    
def get_all_files(startingPath):
    allFiles = []
    for root, dirs, files in os.walk(startingPath):
        for file in files:
            allFiles.append(os.path.join(root, file))   
    return allFiles    